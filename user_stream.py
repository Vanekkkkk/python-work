from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from config import connect
from sys import argv
from pprint import pprint
import json
import logging
from subprocess import run

user_id = argv[1]
db_1 = connect()
data = db_1.table("twitter_data").where("user_id", user_id).first()
campaigns = db_1.table("campaigns").where("user_id", user_id).lists("id")
keywords = db_1.table("tw_keywords").where_in("campaign_id", campaigns).where("rejected", 0).lists("keyword")


class StdOutListener(StreamListener):
    def on_data(self, raw_data):
        data = json.loads(raw_data)
        if 'in_reply_to_status_id' in data:
            # status = Status.parse(self.api, data)
            if self.on_status(data) is False:
                return False
        elif 'delete' in data:
            delete = data['delete']['status']
            if self.on_delete(delete['id'], delete['user_id']) is False:
                return False
        elif 'event' in data:
            # status = Status.parse(self.api, data)
            if self.on_event(data) is False:
                return False
        elif 'direct_message' in data:
            # status = Status.parse(self.api, data)
            if self.on_direct_message(data) is False:
                return False
        elif 'friends' in data:
            if self.on_friends(data['friends']) is False:
                return False
        elif 'limit' in data:
            if self.on_limit(data['limit']) is False:
                return False
        elif 'disconnect' in data:
            if self.on_disconnect(data['disconnect']) is False:
                return False
        elif 'warning' in data:
            if self.on_warning(data['warning']) is False:
                return False
        else:
            logging.error("Unknown message type: " + str(raw_data))

    def on_status(self, status):
        print('new status')
        run(["php", "/var/www/html/artisan", "new:event", "filter", json.dumps(status), json.dumps(list(campaigns))])
        return True

    def on_event(self, event):
        print('new event')
        pprint(event)
        return True

    def on_delete(self, status_id, user_id):
        print('this status has been deleted')
        print(status_id)
        print(user_id)
        return True

    def on_error(self, message):
        print('new error')
        print(message)
        return True

    def on_disconnect(self, notice):
        print('disconnect, BITCH')
        print(notice)

    def on_warning(self, warning):
        print('Warning MAZAFUCKA!')
        print(warning)

    def on_direct_message(self, status):
        print('new DM!')
        print(status)
        return True

    def on_friends(self, friends):
        print('this is my friends')
        print(len(friends))
        return True


if __name__ == '__main__':
    listener = StdOutListener()
    auth = OAuthHandler("LFwA2kotUEMkyJrkGyGkQCSiI", "sohlRsY7om8pk8jEXYABIWBbYtYqN7EqL0gHh83rR6wQoXa3r9")
    auth.set_access_token("760470918623789056-LnqHBQqOZ6WzPFljaAIkyG8WHnB9WUj",
                          "NowauHM7EoBMt62ZZy8QSpp0aFno7SSBZLOww8lw7Sf1Z")
    stream = Stream(auth, listener)
    # stream.filter(track=keywords, async=True)
    # stream.filter(follow=data["tw_id"], async=True)
    stream.userstream(track=["760470918623789056"], async=True)
