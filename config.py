from orator import DatabaseManager


def connect():
    config = {
        'mysql': {
            'driver': 'mysql',
            'host': 'localhost',
            'database': 'testdb',
            'user': 'root',
            'password': 'root',
            'prefix': '',
            'log_queries': True
        }
    }
    return DatabaseManager(config)


def connect_py_db():
    config = {
        'mysql': {
            'driver': 'mysql',
            'host': 'localhost',
            'database': 'pythontest',
            'user': 'root',
            'password': 'root',
            'prefix': '',
            'log_queries': True
        }
    }
    return DatabaseManager(config)


def get_test_keys():
    consumer_key = "aqI9qWXSpIJiZnAJetUNhXDpn"
    consumer_secret = "prFaOAtkT9PvvyVApOdwDJewxCP7C1u9VE1OGBvZv8JhXCTH7R"
    token = "778860524440326144-27n1j4bnlPm0zWXm2AnHxZIGZ1iEbXx"
    token_secret = "scZ9pSPuvjYW5Myy4dfhKHPKEv7H7MWLgY3cOtgz7hsEy"

    return consumer_key, consumer_secret, token, token_secret
