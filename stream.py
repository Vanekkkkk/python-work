from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import tweepy
from config import connect
from sys import argv
from pprint import pprint
import json
from subprocess import run

user_id = argv[1]
db = connect()
data = db.table("twitter_data").where("user_id", user_id).first()
campaigns = db.table("campaigns").where("user_id", user_id).lists("id")
keywords = db.table("tw_keywords").where_in("campaign_id", campaigns).where("rejected", 0).lists("keyword")


class StdOutListener(StreamListener):
    def on_status(self, status):
        # run(["php", "/var/www/html/artisan", "newTweet", "filter", status, json.dumps(list(campaigns))])
        pprint(json.loads(status))
        return True

    def on_error(self, status_code):
        print(status_code)


if __name__ == '__main__':
    listener = StdOutListener()
    auth = OAuthHandler("LFwA2kotUEMkyJrkGyGkQCSiI", "")
    auth.set_access_token("", "")
    stream = Stream(auth, listener)
    stream.filter(track=keywords, async=True)

